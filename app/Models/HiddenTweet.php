<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HiddenTweet extends Model
{

    protected $fillable = ['user_id', 'tweet_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}