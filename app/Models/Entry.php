<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Entry extends Model
{

    protected $fillable = ['title', 'content', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * @param User $user
     * @param bool $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function firstRows(User $user, $limit = true)
    {
        $query = $this->where('user_id', $user->id)->orderBy('created_at', 'DESC');
        if ($limit) {
            $query->limit(env('LIMIT', 3));
        }
        $query->with('user');
        return $query->get();
    }

}