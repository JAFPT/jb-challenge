<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\HiddenTweet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Thujohn\Twitter\Facades\Twitter;

class HomeController extends Controller
{
    private $user;
    private $entry;
    private $hiddenTweet;

    /**
     * HomeController constructor.
     *
     * @param User        $user
     * @param Entry       $entry
     * @param HiddenTweet $hiddenTweet
     */
    public function __construct(
        User $user,
        Entry $entry,
        HiddenTweet $hiddenTweet
    ) {
        $this->middleware('auth')->except(['index', 'show']);
        $this->user = $user;
        $this->entry = $entry;
        $this->hiddenTweet = $hiddenTweet;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = [];
        $users = $this->user->all();
        foreach ($users as $user) {
            $auxEntries = $this->entry->firstRows($user);
            foreach ($auxEntries as $auxEntry) {
                $entries[$auxEntry->created_at->timestamp] = $auxEntry;
            }
        }
        krsort($entries);

        return view('home', compact('entries'));
    }

    /**
     * @param $user_id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($user_id)
    {
        $user = $this->user->find($user_id);

        $hiddenTweets = $this->hiddenTweet->where('user_id', $user_id)->get();
        $allTweets = json_decode(Twitter::getUserTimeline([
            'screen_name' => $user->twitter_username,
            'format'      => 'json'
        ]));

        if (Auth::user()) {
            $twitters = $user_id != Auth::user()->id
                ? $this->deleteTweets($hiddenTweets, $allTweets)
                : $this->hideTweets($hiddenTweets, $allTweets);
        } else {
            $twitters = $this->deleteTweets($hiddenTweets, $allTweets);
        }


        $entries = $this->entry->firstRows($user, false);

        return view('profile', compact('entries', 'user', 'twitters'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function hidden(Request $request)
    {
        $tweetExist = $this->hiddenTweet->where('tweet_id',
            $request->get('tweet_id'))
            ->first();
        if ($tweetExist) {
            $tweetExist->delete();
        } else {
            $hiddenTweet = new HiddenTweet();
            $hiddenTweet->fill($request->all());
            $hiddenTweet->user_id = Auth::user()->id;
            $hiddenTweet->save();
        }

        return response()->json(true);
    }

    /**
     * @param $hiddenTweets
     * @param $twitters
     *
     * @return mixed
     */
    private function deleteTweets($hiddenTweets, $twitters)
    {
        $auxTweets = $twitters;
        foreach ($hiddenTweets as $hidden_tweet) {
            foreach ($auxTweets as $key => $tweet) {
                if ($hidden_tweet->tweet_id == $tweet->id) {
                    unset($auxTweets[$key]);
                } else {
                    $auxTweets[$key]->hiddenTweet = false;
                }
            }
        }

        return $auxTweets;
    }

    /**
     * @param $hiddenTweets
     * @param $twitters
     *
     * @return mixed
     */
    private function hideTweets($hiddenTweets, $twitters)
    {
        $auxTweets = $twitters;
        foreach ($hiddenTweets as $hidden_tweet) {
            foreach ($auxTweets as $key => $tweet) {
                if ($hidden_tweet->tweet_id == $tweet->id) {
                    $auxTweets[$key]->hiddenTweet = true;
                } else {
                    if ( ! isset($auxTweets[$key]->hiddenTweet)) {
                        $auxTweets[$key]->hiddenTweet = false;
                    }
                }
            }
        }

        return $auxTweets;
    }
}
