<?php

namespace App\Http\Controllers;


use App\Models\Entry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class EntryController extends Controller
{

    private $entry;

    /**
     * EntriesController constructor.
     *
     * @param Entry $entry
     */
    public function __construct(Entry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $entries = Entry::select([
            'entries.id',
            'entries.title',
            'entries.content',
            'entries.created_at',
        ])->where('entries.user_id', Auth::user()->id);

        return Datatables::of($entries)->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('app.entry.create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->entry->create([
            'title'   => $request->get('title'),
            'content' => $request->get('content'),
            'user_id' => Auth::user()->id,
        ]);

        return redirect('profile/'.Auth::user()->id)
            ->with('success', 'Success', ['entity' => 'Post']);
    }

    /**
     * @param Entry $entry
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Entry $entry)
    {
        return view('app.entry.show', compact('entry'));
    }


    /**
     * @param Entry $entry
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Entry $entry)
    {
        return view('app.entry.edit', compact('entry'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $post = $this->entry->find($request->get('id'));
        $post->fill($request->all());
        $post->save();

        return redirect('profile/'.Auth::user()->id)
            ->with('success', 'Success', ['entity' => 'Post']);
    }

}