<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/profile/{user_id}', 'HomeController@show');
Route::post('/profile/hidden', array('as' => 'profile.hidden', 'uses' => 'HomeController@hidden'));

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/entry/get_data', array('as' => 'entry.get_data', 'uses' => 'EntryController@getData'));
    Route::resource('entry', 'EntryController');
});