-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 192.168.10.10    Database: jb-challenge
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entries_user_id_foreign` (`user_id`),
  CONSTRAINT `entries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
INSERT INTO `entries` VALUES (1,'HELLO WORD!','First Post Test!',1,'2017-07-31 01:22:38','2017-07-31 03:24:44'),(2,'HELLO JOBSITY','This is a test post!',1,'2017-07-31 01:27:00','2017-07-31 03:25:21'),(3,'HELLO QUITO!','This a test post for QUITO!',1,'2017-07-31 03:25:52','2017-07-31 03:25:52'),(4,'Hello Cuski!','Testing, testing. . . \r\n1\r\n2\r\n3',2,'2017-07-31 03:52:07','2017-07-31 06:41:12'),(5,'Good Morning!','This is a beautiful day!',2,'2017-07-31 03:55:54','2017-07-31 06:41:46'),(6,'Don\'t have any ideas!','In spanish: \"Se me acabaron las ideas\" :)',2,'2017-07-31 03:56:24','2017-08-01 02:17:51'),(7,'Good Job!','Hi babe, good job',2,'2017-07-31 03:56:51','2017-07-31 06:42:43'),(8,'HELLO SUDAMERICA!','This is a test!',1,'2017-07-31 05:37:33','2017-08-01 05:00:41'),(9,'Hello it´s me','Let´s make some shampoo',2,'2017-08-01 02:17:10','2017-08-01 02:17:10'),(10,'TO NIGHT!','Is this a song? jejeje',1,'2017-08-01 05:03:45','2017-08-01 05:03:45'),(11,'Hello I\'m Antho!','Hello Word!',3,'2017-08-01 05:51:54','2017-08-01 05:51:54');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hidden_tweets`
--

DROP TABLE IF EXISTS `hidden_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hidden_tweets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `tweet_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hidden_tweets_user_id_foreign` (`user_id`),
  CONSTRAINT `hidden_tweets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hidden_tweets`
--

LOCK TABLES `hidden_tweets` WRITE;
/*!40000 ALTER TABLE `hidden_tweets` DISABLE KEYS */;
INSERT INTO `hidden_tweets` VALUES (6,2,881680498212438016,'2017-08-01 04:38:49','2017-08-01 04:38:49'),(7,1,877886606463520770,'2017-08-01 05:07:33','2017-08-01 05:07:33'),(8,1,855487570419089409,'2017-08-01 05:07:35','2017-08-01 05:07:35'),(10,3,891670870472761345,'2017-08-01 05:53:11','2017-08-01 05:53:11');
/*!40000 ALTER TABLE `hidden_tweets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_07_30_161423_create_entries_table',1),(8,'2014_10_12_000000_create_users_table',1),(9,'2014_10_12_100000_create_password_resets_table',1),(10,'2017_07_30_161423_create_entries_table',1),(11,'2017_08_01_030643_create_hidden_tweets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_twitter_username_unique` (`twitter_username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sebastian Aleman','sebas.alem.f@gmail.com','$2y$10$9GOAK/EimYA0brRBb0lK5OUYRZmTLZQXfa9GrwCAoWjXSGtQSZSdS','@SebasAlemanF','DA2UQL3cYL03KSAhIJ7zVSRTLyGVRnrvk4lAi3tCzpDWHaaCVnRrP8rOA0ms','2017-07-30 22:22:23','2017-07-30 22:22:23'),(2,'Valeria Coral','valeriacoral12@hotmail.com','$2y$10$IIv4T/TScIEbY9/CwfksiujyQv7j1GtFUfSS1G6.IVuTpDz3ZZyQ2','@Valenena12','gPi6Mj0o4GgZ4OTgMhqgRm8kOMZbv42ww31VooNXAKMwFEZDeeUUiD5tqElM','2017-07-31 03:47:24','2017-07-31 06:40:30'),(3,'Antho Arcos','anthoArcAlem@gmail.com','$2y$10$KEXMGWI.KLe5b8sMkWeCnOlQdMkSl.jdL6ZYLVP5b3nSgt80sruoe','@AnthonelaArcos',NULL,'2017-08-01 05:51:12','2017-08-01 05:51:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-01 17:47:03
