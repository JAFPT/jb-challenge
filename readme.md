#INSTRUCTIONS
    This app was created with Laravel 5.4
    Extract all content in /var/www/html

##DATABASE
    Create database 'jb-challenge'

##VIRTUAL HOST

    <VirtualHost *:80>
    ServerName sebastian-aleman.jobsitychallenge.com
    DocumentRoot "/var/www/html/sebastian-aleman/public"
    <Directory "/var/www/html/sebastian-aleman/public">
      AllowOverride all
      </Directory>
    </VirtualHost>

##LARAVEL CONFIG DATABASE
    In .env file change this:
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=jb-challenge
        DB_USERNAME=homestead
        DB_PASSWORD=secret

##LARAVEL COMPONENTS AND MIGRATIONS
    $ cd /var/www/html/sebastian-aleman
    $ composer install
    $ php artisan migrate
    $ npm install
    $ php artisan laroute:generate
    $ npm run dev

##RESTART DATABASE EXAMPLE RECORDS
    Restart database data backup, this file is in 
    /var/www/html/sebastian-aleman/DATABASE-SCRIPT/jb-challenge-data-20170801.sql
