'use strict';

var app = angular.module('challengeApp', [
    'directive.dataTable',
    'directive.select2',
    'challengeApp.controllers',
    'challengeApp.entry'
]);
