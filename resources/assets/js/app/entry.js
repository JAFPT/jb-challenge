angular.module('challengeApp.entry', ['challengeApp.controllers'])

    .controller('EntriesGrid', ['$scope', function ($scope) {
        $scope.dataTableOptions = {
            "ajax": laroute.route('entry.get_data'),
            "columns": [
                {data: "id", name: "entries.id"},
                {data: "title", name: "entries.title", sDefaultContent: ""},
                {data: "content", name: "entries.content", sDefaultContent: ""},
                {data: "created_at", name: "entries.created_at", sDefaultContent: ""}
            ],
            "challengeAppFilters": [],
            "challengeAppRoutes": {
                "show": "entry.show",
                "edit": "entry.edit"
            },
            challengeAppParam: "entry"
        };

    }])

    .controller('TwitterList', ['$scope', '$http', function ($scope, $http) {

        showOrHiddenTweet = function (tweet_id) {
            $http.post(laroute.route('profile.hidden'), {
                'tweet_id': tweet_id
            }).then(function (result) {
                console.log(result.data)
            });
        };

        $('.tweet').change(function (event) {
            var tweet_id = event.target.id;
            console.log(tweet_id);
            showOrHiddenTweet(tweet_id);
        });

    }])

    .controller('EntryList', ['$scope', '$http', function ($scope, $http) {

        $scope.paginatorPersonalize = function () {
            $('.pager').empty();
            var listElement = $('#entries');
            var perPage = $scope.all ? listElement.find('li').length : 4;
            var numItems = listElement.find('li').length;
            var numPages = Math.ceil(numItems / perPage);
            $('.pager').data("curr", 0);
            var curr = 0;
            while (numPages > curr) {
                $('<li><a href="#" class="page_link">' + (curr + 1) + '</a></li>').appendTo('.pager');
                curr++;
            }
            $('.pager .page_link:first').addClass('active');

            listElement.children().css('display', 'none');
            listElement.children().slice(0, perPage).css('display', 'block');

            function goTo(page) {
                var startAt = page * perPage,
                    endOn = startAt + perPage;

                listElement.children().css('display', 'none').slice(startAt, endOn).css('display', 'block');
                $('.pager').attr("curr", page);
            }

            $('.pager li a').click(function () {
                var clickedPage = $(this).html().valueOf() - 1;
                goTo(clickedPage, perPage);
            });
            $scope.all = !$scope.all;
        };
        // Init pagination
        $scope.paginatorPersonalize();
    }])
;