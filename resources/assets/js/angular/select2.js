'use strict';

var select2 = angular.module('directive.select2', []);

    select2.directive('select2', function($http) {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                var options = {
                    allowClear:false,
                    placeholder: "Select a item"
                };

                // extend options from controller
                options = angular.extend(options, scope[attrs.select2]);

                var challengeCallback = null;
                if (options.hasOwnProperty("challengeCallback")) {
                    challengeCallback = options.challengeCallback;
                    delete options.challengeCallback;
                }

                // create object with options
                element.removeClass('form-control');
                var object = element.select2(options);

                if (attrs.ngModel) {
                    scope.$watch(attrs.ngModel, function (value) {
                        if (!value) {
                            element.val("");
                        }
                    });
                }

                // listen for changes, communicate with datatable directive
                element.bind('click', function(element, test) {

                    if (challengeCallback != null) {
                        var fn = scope[challengeCallback];
                        fn($( this ));
                    }
                });

            }
        };
    });
    
    select2.directive('select2Datatable', function($http) {
        return {
            restrict: "A",

            require: "^challengeDataTable",

            link: function (scope, element, attrs, challengeDataTableController) {
                var options = {
                    allowClear:false,
                    placeholder: "Select a item",
                    challengeFilterName: ""
                };

                // extend options from controller
                options = angular.extend(options, scope[attrs.select2Datatable]);

                // create object with options
                element.removeClass('form-control');
                var object = element.select2(options);     

                // listen for changes, communicate with datatable directive
                element.bind('click', function(element, test) {    
                    challengeDataTableController.reload({ "name": options.challengeFilterName, "value": $( this ).val() });
                });
            }
        };
    });