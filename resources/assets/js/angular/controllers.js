'use strict';

/* Controllers */
angular.module('challengeApp.controllers', [])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    })

    .factory('httpRequestInterceptor', function () {
        return {
            request: function (config) {
                config.headers['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
                return config;
            }
        };
    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
    });