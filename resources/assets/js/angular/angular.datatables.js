'use strict';

var dataTable = angular.module('directive.dataTable', []);
    
    dataTable.directive('challengeAppManualDataTable', function() {
        return function(scope, element, attrs) {

            // apply DataTable options, use defaults if none specified by user
            var options = {
                "sDom": "<'row'<'col-lg-6'l> <'col-lg-6'Tf> r> <'row'<'col-lg-12 manual-table't> <'row'<'col-lg-6'i>> <'row'<'col-lg-12 center'p>>",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ registers per page",
                    "sZeroRecords": "Sorry, no results found.",
                    "sInfoEmpty": "Empty",
                    "sSearch": "Search: ",
                    "oPaginate":{
                        "sNext": "Next",
                        "sPrevious": "Preview",
                        "sFirst": "First",
                        "sLast": "Last"
                    }
                },
                "oTableTools": {
                    "sSwfPath": "/swf/copy_csv_xls_pdf.swf",
                    "aButtons":    [{
                        "sExtends": "xls",
                        "sButtonText": "Exportar a Excel"
                    }]
                }
            };
            
            // extend options from controller
            if (attrs.challengeAppManualDataTable.length > 0) {
                options = angular.extend(options, scope[attrs.challengeAppManualDataTable]);
            }

            // Tell the dataTables plugin what columns to use
            // We can either derive them from the dom, or use setup from the controller           
            var explicitColumns = [];
            element.find('th').each(function(index, elem) {
                explicitColumns.push($(elem).text());
            });
            if (explicitColumns.length > 0) {
                options["aoColumns"] = explicitColumns;
            } else if (attrs.aoColumns) {
                options["aoColumns"] = scope.$eval(attrs.aoColumns);
            }

            // aoColumnDefs is dataTables way of providing fine control over column config
            if (attrs.aoColumnDefs) {
                options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
            }
            
            if (attrs.fnRowCallback) {
                options["fnRowCallback"] = scope.$eval(attrs.fnRowCallback);
            }

            // apply the plugin
            var dataTable = element.find('table').dataTable(options);

            var el = element;
            
            // dress default results select
            element.find('.dataTables_length select').select2({
                minimumResultsForSearch: 10
            });
            
            // watch for any changes to our data, rebuild the DataTable
            scope.$watch(attrs.aaData, function(value) {
                var val = value || null;
                var data = scope.$eval(attrs.aaData);
                if (val) {
                    if (data.length > 0) {
                        if (attrs.aoColumns) {
                            options["aoColumns"] = scope.$eval(attrs.aoColumns);
                        }

                        // aoColumnDefs is dataTables way of providing fine control over column config
                        if (attrs.aoColumnDefs) {
                            options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
                        }
                        
                        options["aaData"] = scope.$eval(attrs.aaData);
                        dataTable.fnDestroy();
                        el.find('table').empty();
                        el.find('table').dataTable(options);
                        
                        el.find('.dataTables_length select').select2({
                            minimumResultsForSearch: 10
                        });
                        
                    }
                   // dataTable.fnClearTable();
                   // dataTable.fnAddData(scope.$eval(attrs.aaData));
                }
            });
        };
    });
    
    dataTable.directive('challengeAppDataTable', function($http) {
        return {

            restrict: "A",

            controller: function($scope) {
                this.reload = function(filter) {

                    if ( Object.prototype.toString.call( filter.value ) === '[object Array]' ) {
                        filter.value = filter.value.join(",");
                    }
                    // current settings for datatable
                    var oSettings = $scope.challengeAppDataTable.fnSettings();

                    // search challengeAppFilters and replace with current filter
                    for (var i = 0; i < oSettings.oInit.challengeAppFilters.length; i++) {
                        if (oSettings.oInit.challengeAppFilters[i].name === filter.name ) {
                            oSettings.oInit.challengeAppFilters[i].value = filter.value;
                        }
                    }

                    // push challengeAppFilters again
                    oSettings.aoServerParams[0].fn = function(aoData) {
                        for (var i = 0; i < oSettings.oInit.challengeAppFilters.length; i++) {
                            aoData[oSettings.oInit.challengeAppFilters[i].name] = oSettings.oInit.challengeAppFilters[i].value;
                        }
                    };

                    // push challengeAppFilters to excel exporter
                    oSettings.oInit.oTableTools.aButtons[0].sExtraData = function() {
                        return oSettings.oInit.challengeAppFilters;
                    };
                    
                    // reload the whole datable with data from server
                    $scope.challengeAppDataTable.fnReloadAjax();
                };

                this.getData = function() {
                    return $scope.challengeAppDataTable.fnGetData();
                };
            },

            link: function ($scope, element, attrs) {

                // Showing or hidding "export" and "search" options for the datatable. By default everything is showed
                var searchFilter = "f"; var exportBtn = "T";
                if ($scope[attrs.challengeAppDataTable].hasOwnProperty("challengeAppConf")) {
                    
                    if ($scope[attrs.challengeAppDataTable].challengeAppConf.hasOwnProperty("exportBtn")) {
                        if($scope[attrs.challengeAppDataTable].challengeAppConf.exportBtn === false)
                            exportBtn = "";
                    }
                    if ($scope[attrs.challengeAppDataTable].challengeAppConf.hasOwnProperty("searchFilter")) {
                        if($scope[attrs.challengeAppDataTable].challengeAppConf.searchFilter === false)
                            searchFilter = "";
                    }
                }
                
                // default options for challengeApp
                var options = {
                    "sDom": "<'row'<'col-lg-6'l> <'col-lg-6'"+exportBtn+searchFilter+"> r> <'row'<'col-lg-12 table-responsive't>> <'row'<'col-lg-6'i>> <'row'<'col-lg-12 center'p>>",
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ registers per page",
                        "sZeroRecords": "Sorry, no results found.",
                        "sInfoEmpty": "Empty",
                        "sSearch": "Search:",
                        "oPaginate":{
                            "sNext": "Next",
                            "sPrevious": "Preview",
                            "sFirst": "First",
                            "sLast": "Last"
                        }
                    },
                    "processing": true,
                    "serverSide": true,
                    "columnDefs": [],
                    "fnDrawCallback": function( oSettings ) {
                        $(".filters_container").removeClass('hide');
                     },                     
                    "oTableTools": {
                        "sSwfPath": "/swf/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            {
                                "sExtends": "customxls",
                                "sButtonText": "<i class='fa fa-download'></i> Exportar",
                                "sExtraData": function() {
                                    return true;
                                },
                                fnClick: function(button, oConfig) {
                                    var source = this.s.dt.sAjaxSource;
                                    var oParams = this.s.dt.oApi._fnAjaxParameters( this.s.dt );

                                    if (oParams[4].name === "iDisplayLength") {
                                        oParams[4].value = this.s.dt.fnRecordsDisplay();
                                    }
                                    
                                    oParams = oParams.concat(this.s.dt.oInit.challengeAppFilters);
                                    var src = source+'/excel'+"?"+window.$.param(oParams);

                                    var downloadURL = function downloadURL(url) {
                                        var hiddenIFrameID = 'hiddenDownloader',
                                            iframe = document.getElementById(hiddenIFrameID);
                                        if (iframe === null) {
                                            iframe = document.createElement('iframe');
                                            iframe.id = hiddenIFrameID;
                                            iframe.style.display = 'none';
                                            document.body.appendChild(iframe);
                                        }
                                        iframe.src = url;
                                    };

                                    downloadURL(src);
                                }
                            }
                        ]
                    }
                };

                // extend options from controller
                options = angular.extend(options, $scope[attrs.challengeAppDataTable]);
                      
                // add actions column (convention: aoColumns should exist by now)
                if (options.hasOwnProperty("challengeAppRoutes") && options.hasOwnProperty("challengeAppParam")) {
                    var route_params = [];
                    options.columnDefs.push(
                        {
                          "data": null,
                          "searchable": false,
                          "sortable": false,
                          "targets": options.columns.length,
                          "render": function ( data, type, full ) {
                            var template = '<div class="datatable_actions">';
                            route_params[options.challengeAppParam] = full.id;

                                var showAction = false;
                                if(typeof $scope.initVars == "undefined" || typeof $scope.initVars.actions == "undefined"){
                                    showAction = true;
                                }

                                // If row has been removed
                                if (full.deletedAt) {
                                    if (options.challengeAppRoutes.hasOwnProperty("confirmRemoveDelete")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.confirmRemoveDelete == "undefined"){
                                            var confirmRemoveDelete = laroute.route(options.challengeAppRoutes.confirmRemoveDelete, { id: data});
                                            template += '<a data-toggle="tooltip" title="Unlock" class="btn btn-warning" href="'+confirmRemoveDelete+'"><i class="icon-unlock"></i></a>';
                                        }
                                    }
                                } else {

                                    if (options.challengeAppRoutes.hasOwnProperty("show")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.show == "undefined"){
                                            var showRoute = laroute.route(options.challengeAppRoutes.show, $.extend(true, {}, route_params));
                                            template += '<a data-toggle="tooltip" title="View" class="btn btn-circle btn-info" style="margin-right:5px;" href="'+showRoute+'"><i class="fa fa-search-plus"></i></a>';
                                        }
                                    }

                                    if (options.challengeAppRoutes.hasOwnProperty("edit")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.edit == "undefined"){
                                            var editRoute;
                                            if (options.challengeAppRoutes.edit.hasOwnProperty("route")) {
                                                editRoute = laroute.route(options.challengeAppRoutes.edit.route, $.extend(true, {}, route_params));
                                                if (options.challengeAppRoutes.edit.hasOwnProperty("conditionToShow")) {
                                                    if(options.challengeAppRoutes.edit.conditionToShow(full)){
                                                        template += '<a data-toggle="tooltip" title="Edit" class="btn btn-circle btn-warning" style="margin-right:5px;" href="'+editRoute+'"><i class="fa fa-edit"></i></a>';
                                                    }
                                                } else {
                                                    template += '<a data-toggle="tooltip" title="Edit" class="btn btn-circle btn-warning" style="margin-right:5px;" href="'+editRoute+'"><i class="fa fa-edit"></i></a>';
                                                }
                                            } else {
                                                editRoute = laroute.route(options.challengeAppRoutes.edit, $.extend(true, {}, route_params));
                                                template += '<a data-toggle="tooltip" title="Edit" class="btn btn-circle btn-warning" style="margin-right:5px;" href="'+editRoute+'"><i class="fa fa-edit"></i></a>';
                                            }
                                        }
                                    }

                                    if (options.challengeAppRoutes.hasOwnProperty("prescription")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.prescription == "undefined"){
                                            var prescriptionRoute = laroute.route(options.challengeAppRoutes.prescription, $.extend(true, {}, route_params));
                                            template += '<a data-toggle="tooltip" title="Receta" class="btn btn-circle btn-success" style="margin-right:5px;" href="'+prescriptionRoute+'"><i class="fa fa-file-text"></i></a>';
                                        }
                                    }

                                    if (options.challengeAppRoutes.hasOwnProperty("void")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.void == "undefined"){
                                            var voidRoute = laroute.route(options.challengeAppRoutes.void, $.extend(true, {}, route_params));
                                            template += '<a data-toggle="tooltip" title="Anular" class="btn btn-circle btn-danger" style="margin-right:5px;" href="'+voidRoute+'"><i class="fa fa-times"></i></a>';
                                        }
                                    }

                                    if (options.challengeAppRoutes.hasOwnProperty("exportErrors")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.exportErrors == "undefined"){
                                            if (full.errors) {
                                                var exportErrorsRoute = laroute.route(options.challengeAppRoutes.exportErrors.route, { id: data});
                                                template += '<a data-toggle="tooltip" title="Export Errors" class="btn btn-circle btn-danger" style="margin-right:5px;" href="'+exportErrorsRoute+'"><i class="fa fa-warning"></i></a>';
                                            }
                                        }
                                    }

                                    if (options.challengeAppRoutes.hasOwnProperty("changePassword")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.changePassword == "undefined"){
                                            var changePasswordRoute;
                                            if (options.challengeAppRoutes.changePassword.hasOwnProperty("route")) {
                                                changePasswordRoute = laroute.route(options.challengeAppRoutes.changePassword.route, { id: data});
                                                if (options.challengeAppRoutes.changePassword.hasOwnProperty("conditionToShow")) {
                                                    if(options.challengeAppRoutes.changePassword.conditionToShow(full)){
                                                        template += '<a data-toggle="tooltip" title="Change Password" class="btn btn-circle btn-danger" style="margin-right:5px;" href="'+changePasswordRoute+'"><i class="fa fa-key"></i></a>';
                                                    }
                                                } else {
                                                    template += '<a data-toggle="tooltip" title="Change Password" class="btn btn-circle btn-danger" style="margin-right:5px;" href="'+changePasswordRoute+'"><i class="fa fa-key"></i></a>';
                                                }
                                            } else {
                                                changePasswordRoute = laroute.route(options.challengeAppRoutes.changePassword, { id: data});
                                                template += '<a data-toggle="tooltip" title="Change Password" class="btn btn-circle btn-danger" style="margin-right:5px;" href="'+changePasswordRoute+'"><i class="fa fa-key"></i></a>';
                                            }
                                        }
                                    }
                                    if (options.challengeAppRoutes.hasOwnProperty("confirmDelete")) {
                                        if(showAction || !showAction && typeof $scope.initVars.actions.confirmDelete == "undefined"){
                                            var confirmDelete = laroute.route(options.challengeAppRoutes.confirmDelete, { id: data});
                                            template += '<a data-toggle="tooltip" title="Delete" class="btn btn-circle btn-danger" href="'+confirmDelete+'"><i class="fa fa-trash-o"></i></a>';
                                        }
                                    }
                                }

                            template += "</div>";

                            return template;
                          }
                        }
                    );
                }
                

                // add Filters
                if (options.hasOwnProperty("challengeAppFilters") && options.challengeAppFilters.length > 0)  {
                    options.fnServerParams = function ( aoData ) {
                        Array.prototype.push.apply(aoData, options.challengeAppFilters);
                    };

                    options.oTableTools.aButtons[0].sExtraData = function() {
                        return options.challengeAppFilters;
                    };
                }

                if (options.hasOwnProperty("challengeAppAutoRefresh") && options.challengeAppAutoRefresh)  {
                    setInterval(function(){
                       $scope.challengeAppDataTable.fnReloadAjax();
                    },30000);
                }
                // default search value
                if (attrs.searchValue !== "") {
                    options.oSearch = {
                        "sSearch": attrs.searchValue
                    };
                }

                // create datatable with options
                $scope.challengeAppDataTable = element.find('table').dataTable(options);
                
                // dress default results select
                element.find('.dataTables_length select').select2({
                    minimumResultsForSearch: 10
                });

                /*
                // watch for any changes to our data, rebuild the DataTable
                scope.$watch(attrs.aaData, function(value) {
                    var val = value || null;
                    if (val) {
                        dataTable.fnClearTable();
                        dataTable.fnAddData(scope.$eval(attrs.aaData));
                    }
                });
                */
            }
        };

    });