@extends('layouts.app')

@section('content')
    <div class="row" ng-app="challengeApp">
        <div class="col-md-8 col-md-offset-2" ng-controller="EntryList">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>General Dashboard</h2>
                </div>

                <div class="panel-body">
                    <ul class="list-group" id="entries">
                        @foreach($entries as $entry)
                            <li class="list-group-item">
                                <a href="/profile/{!! $entry->user->id !!}">
                                    <span class="badge pull-right">
                                        {!! $entry->user->username !!}
                                    </span>
                                </a>
                                <br>
                                <p class="list-group-item-text">
                                    <b>{!! $entry->title !!}</b>
                                    <br>
                                    {!! $entry->content !!}
                                    <small class="pull-right">
                                        {!! $entry->created_at !!}
                                    </small>
                                </p>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="pager">
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
