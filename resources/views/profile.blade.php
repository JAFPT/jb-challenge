@extends('layouts.app')

@section('content')
    <div class="row" ng-app="challengeApp">
        <div class="col-md-8" ng-controller="EntryList">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>{!! $user->username !!}'s Dashboard </h2>
                </div>

                <div class="panel-body">
                    <ul class="list-group" id="entries">
                        @foreach($entries as $entry)
                            <li class="list-group-item">
                                <a href="#">
                                    <span class="badge pull-right">
                                        {!! $entry->user->username !!}
                                    </span>
                                </a>
                                <br>
                                <p class="list-group-item-text">
                                    <b>{!! $entry->title !!}</b>
                                    <br>
                                    {!! $entry->content !!}
                                    <small class="pull-right">
                                        {!! $entry->created_at !!}
                                    </small>
                                </p>

                                @if(Auth::user())
                                    @if(Auth::user()->id == $user->id)
                                        <a href="/entry/{!! $entry->id !!}" class="btn pull-right" title="View">
                                            <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                        </a>
                                        <a href="/entry/{!! $entry->id !!}/edit" class="btn pull-right" title="Edit">
                                            <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
                                        </a>
                                        <br>
                                    @endif
                                @endif
                                <br>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="pager">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Twitters </h2>
                </div>

                <div class="panel-body" ng-controller="TwitterList" id="tweets">
                    <ul class="list-group">
                        @foreach($twitters as $twitter)
                            <li class="list-group-item">
                                <a href="#">
                                    <span class="badge pull-right">
                                        {!! '@' . $twitter->user->screen_name !!}
                                    </span>
                                </a>
                                <img src="{!! $twitter->user->profile_image_url!!}" alt="Twitter"
                                     class="img-rounded img-responsive">
                                <p>
                                    <b>{!! $twitter->text !!}</b>
                                    <br>
                                    <small class="pull-right">
                                        {!! $twitter->created_at !!}
                                    </small>
                                </p>
                                @if(Auth::user())
                                    @if(Auth::user()->id == $user->id)
                                        <input class="tweet"
                                               type="checkbox"
                                               {!! isset($twitter->hiddenTweet)?!$twitter->hiddenTweet? 'checked': '':'checked' !!}
                                               data-toggle="toggle"
                                               data-size="mini"
                                               id="{!! $twitter->id !!}">
                                    @endif
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection