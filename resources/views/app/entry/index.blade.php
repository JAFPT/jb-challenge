@extends('layouts.app')
@section('content')
    <div class="row" ng-app="challengeApp">
        <div class="col-lg-12" ng-controller="EntriesGrid">
            <div class="widget" challenge-app-data-table="dataTableOptions">
                <div class="widget-head text-center">
                    <div class="pull-left">List Posts</div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <table class="table table-striped bootstrap-datatable datatable">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection