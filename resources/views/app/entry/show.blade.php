@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="widget">
                <div class="widget-head text-center">
                    <div class="pull-left">Show Post</div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('title', 'Title') !!}
                                    <p>{!! $entry->title !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('content', 'Content') !!}
                                    <p>{!! $entry->content !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('created_at', 'Created At') !!}
                                    <p>{!! $entry->created_at !!}</p>
                                </div>
                            </div>
                        </div>
                        {!! link_to_route('entry.index', 'Cancel', [], ['class' => 'btn'])  !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection