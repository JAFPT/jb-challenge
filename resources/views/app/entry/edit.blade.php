@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="widget">
                <div class="widget-head text-center">
                    <div class="pull-left">Edit Post</div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        {!! Form::open(['url' => '/entry/'.$entry->id, 'method' => 'PUT']) !!}
                        {!! Form::hidden('id', $entry->id) !!}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('title', 'Title') !!}
                                    {!! Form::text('title', $entry->title, ['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('content', 'Content') !!}
                                    {!! Form::textarea('content', $entry->content, ['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection