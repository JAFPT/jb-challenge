@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="widget">
                <div class="widget-head text-center">
                    <div class="pull-left">Create Post</div>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                    <div class="padd">
                        {!! Form::open(['url' => '/entry']) !!}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('title', 'Title') !!}
                                    {!! Form::text('title', '', ['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('content', 'Content') !!}
                                    {!! Form::textarea('content', '', ['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Post', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection