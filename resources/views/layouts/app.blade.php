<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
@include('layouts.partials.navbar')

<div class="content">
    @include('layouts.partials.sidebar')
    <div class="mainbar">
        <!-- Matter -->
        <div class="matter">
            <div class="container" ng-app="challengeApp">
                @if (Session::has('error'))
                    <div class="alert alert-error alert-danger">{{ Session::get('error') }}</div>
                @endif

                @if (Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif

                @if (Session::has('notice'))
                    <div class="alert alert-info">{{ Session::get('notice') }}</div>
                @endif
                @yield('content')
            </div>
        </div>
        <!-- Matter ends -->
        <div class="clearfix"></div>
    </div>
</div>
@include('layouts.partials.footer')

<!-- Scripts -->
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
