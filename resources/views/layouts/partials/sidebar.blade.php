<!-- Sidebar -->
<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <!--- Sidebar navigation -->
    <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
    <ul id="nav">
        <li class="open">
            <a href="/"><i class="fa fa-home"></i> Home</a>
        </li>
        @if(!Auth::user())
            <a href="/login" class="btn btn-danger btn-block">Please Log In</a>
        @else
        <!-- Main menu with font awesome icon -->
            <li class="">
                <a href="/profile/{!! Auth::user()->id !!}"><i class="fa fa-list"></i> My Posts</a>
            </li>
            <li class="">
                <a href="/entry/create"><i class="fa fa-plus"></i> New Post</a>
            </li>
        @endif
    </ul>
</div>

<!-- Sidebar ends -->