const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/css/macadmin.css',
    'resources/assets/css/widgets.css',
    'resources/assets/css/main.css'
], 'public/css/theme.css');

mix.styles([
    'node_modules/select2/dist/css/select2.min.css',
    'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
    'node_modules/bootstrap-toggle/css/bootstrap-toggle.css',
], 'public/css/vendors.css');

mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    'node_modules/select2/dist/js/select2.full.min.js',
    'node_modules/select2/dist/js/i18n/es.js',
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/bootstrap-toggle/js/bootstrap-toggle.js',
    'node_modules/angular/angular.min.js',
    'resources/assets/js/laroute.js',
    'resources/assets/js/angular/*.js',
    'resources/assets/js/app/*.js',
    'resources/assets/js/main.js',
    'resources/assets/js/custom.js',
], 'public/js/main.js');
